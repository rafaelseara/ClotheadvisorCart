import React, { Component } from 'react';
import Header from './components/header/Header';
import Shop from './components/shop/Shop';
import Footer from './components/footer/Footer';

//import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <Header/>
        <Shop/>
        <Footer/>
      </div>

    );
  }
}

export default App;
