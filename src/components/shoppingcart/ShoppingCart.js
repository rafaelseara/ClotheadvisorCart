import React, { Component } from 'react';
import ItemCart from './../shoppingcart/ItemCart';
import CheckoutPrice from './../shoppingcart/CheckoutPrice';
import FooterCheckoutCart from './../shoppingcart/FooterCheckoutCart';
import './../../styles.css';

export default class ShoppingCart extends Component {

    render() {
        return (
            <article className="section-shoppingcart">
                <h2>Your Clotheadvisor shopping cart</h2>
                <form id="shoppingcart-form">
                    <table>
                        <thead>
                            <tr>
                                <th className="product">Product</th>
                                <th className="price">Price</th>
                                <th className="quantity">Quantity</th>
                            </tr>
                            <hr className="lineDashed"/>
                        </thead>
                        <tbody>
                            {this.props.cartItems.map((item) => (
                                <ItemCart 
                                    productId= {item.product.id}
                                    productName= {item.product.name}
                                    productDsc= {item.product.description}
                                    price= {item.product.price}
                                    quantity= {item.quantity}
                                    removeItem= {this.props.removeItem}
                                    updateQuantity= {this.props.updateQuantity}
                                />
                            
                            ))}
                        </tbody>
                        <tfoot>
                            <tr>
                                <CheckoutPrice value={this.calculatePrices(this.props.cartItems)}/>
                            </tr>
                        </tfoot>
                    </table>
                </form>
                <FooterCheckoutCart />
            </article>
        )
    }

    calculatePrices( items ) {
        var price = 0;

        items.forEach((item) => (
            price += item.product.price * item.quantity
        ));

        return price;
    }
}