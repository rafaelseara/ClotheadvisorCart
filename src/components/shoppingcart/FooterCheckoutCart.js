import React, { Component } from 'react';
import './../../styles.css';

export default class FooterCheckoutCart extends Component {

    render() {
        return (
            <footer className="checkoutFooter">
                <p>
                    <em>Preparing your order</em>
                </p>
                <div id="paymentButtons">
                    <a href="#">Continue shopping</a>
                    &nbsp;
                    <em>or</em>
                    &nbsp;
                    <button 
                     type="submit"
                     form="shoppingcart-form">
                     Proceed to Checkout
                    </button>
                </div>
            </footer>
        )
    }
}