import React, { Component } from 'react';

import './../../styles.css';

export default class CheckoutPrice extends Component {

    render() {
        return (
            <td className="checkoutPrice" colSpan="3">
                <ol>
                    <li>
                        <div className="priceWidth">
                            <label>Price before <abbr title="Value Added Tax">VAT</abbr>:</label>
                            <output name="befVat">{parseFloat(this.props.value).toFixed(2)}</output>
                        </div>
                    </li>
                    <li>
                        <div className="priceWidth">
                            <label><abbr title="Value Added Tax">VAT</abbr> @ <strong>20</strong>%:</label>
                            <output name="vat">{parseFloat(this.props.value * 0.2).toFixed(2)}</output>
                        </div>
                    </li>
                    <li>
                        <hr className="line"/>
                    </li>
                    <li>
                        <div className="priceWidth">
                            <label>Total to be paid:</label>
                            <output id="finalPay" name="amount">{parseFloat((this.props.value * 0.2 + this.props.value)).toFixed(2)}</output>
                        </div>
                    </li>
                    <li>
                        <hr className="lineDashed"/>
                    </li>
                </ol>
            </td>
        )
    }
}



