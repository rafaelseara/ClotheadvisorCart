import React, { Component } from 'react';
import './../../styles.css';

export default class ItemCart extends Component {
    
    render() {
        return (
            <tr>
                <tr className="cartItem">
                    <td>
                        <h3>{this.props.productName}</h3>
                        <p>{this.props.productDsc}</p>
                        <div>
                            <button 
                             className="removeButton" 
                             type="button" 
                             id={this.props.productId} 
                             onClick={this.props.removeItem}>
                                Remove
                            </button>
                        </div>
                    </td>
                    <td className="priceColGap">
                        <h3>&euro;{this.props.price}</h3>
                    </td>
                    <td className="inputCell">
                        <input 
                         className="inputQuantity" 
                         type="tel" 
                         name="productQt" 
                         min="1" 
                         value={this.props.quantity} 
                         readOnly>
                        </input>
                        &nbsp;
                        <span>
                            <button 
                             className="quantityButton" 
                             type="button" 
                             aria-label="decrease" 
                             id={this.props.productId} 
                             onClick= {this.props.updateQuantity}>
                                -
                            </button>
                            &nbsp;
                            <button 
                             className="quantityButton"
                             type="button" 
                             aria-label="step up" 
                             id={this.props.productId} 
                             onClick= {this.props.updateQuantity}>
                                +
                            </button>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colSpan="3">
                        <hr className="line"/>
                    </td>
                </tr>
            </tr>
        )
    }
}