import React, { Component } from 'react';
import ShopItem from './../shop/ShopItem';
import ShoppingCart from './../shoppingcart/ShoppingCart';
import './../../styles.css';

export default class Shop extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            products:  [
                {
                    "id": 1,
                    "name": "Cap",
                    "description": "Leather Cap with brand logo",
                    "price": 10
                },
                {
                    "id": 2,
                    "name": "Jeans",
                    "description": "Levi's 511 jeans",
                    "price": 59.99
                },
                {
                    "id": 3,
                    "name": "Dress",
                    "description": "Summer dress",
                    "price": 49.99
                },
                {
                    "id": 4,
                    "name": "Shirt",
                    "description": "Cheap T-shirt",
                    "price": 5.50
                }
            ],
            cart: []
            
        }

        this.handleAddItem = this.handleAddItem.bind(this);
        this.handleRemoveItem = this.handleRemoveItem.bind(this);
        this.handleQuantityUpdt = this.handleQuantityUpdt.bind(this);
    }

    render() {
        return (
            <main>
                <article id="section-shop">
                    <h2>The Clotheadvisor Shop</h2>
                    <p>Select items</p>
                    <p><strong>Please note: All prices are without <abbr title="Value Added Tax">VAT</abbr>!</strong></p>
                    <div id="board">
                        <ol className="productList">
                                {this.state.products.map(
                                    (prd, index) =>
                                    (
                                        <li key={index}> <ShopItem id={prd.id} name={prd.name} dsc={prd.description} price={prd.price} handleAdd={this.handleAddItem}/> </li>
                                    )
                                )}
                        </ol>
                    </div>
                </article>
                {this.state.cart.length > 0 &&
                    <ShoppingCart cartItems= {this.state.cart} removeItem= {this.handleRemoveItem} updateQuantity= {this.handleQuantityUpdt}/>
                }
            </main>
        )
    }

    handleAddItem({ target }) {

        //checks if product is already in the shopping cart
        if(this.state.cart.find( (item) => item.product.id == target.id) === undefined){
            this.setState({
                cart: this.state.cart.concat({
                    "product": this.state.products.find( (item) => item.id == target.id),
                    "quantity": 1
                })
            });
        }else{
            console.log("product is already in the shopping cart");
        }
    }


    handleRemoveItem({ target }) {
        var targetObj = this.state.cart.find( (item) => item.product.id == target.id);
        var index = this.state.cart.indexOf(targetObj);

        this.setState({
            cart: this.state.cart.filter((_, i) => i !== index)
        });
    }


    handleQuantityUpdt({ target }) {
        switch(target.getAttribute('aria-label')) {
            case 'decrease':
                var cartObjs = this.state.cart;
                var targetObj = cartObjs.find( (item) => item.product.id == target.id);
                var index = cartObjs.indexOf(targetObj);
                if(cartObjs[index].quantity - 1 > 0) {
                    cartObjs[index].quantity = cartObjs[index].quantity - 1;
                    this.setState({cart: cartObjs});
                }
                break;
            case 'step up':
                var cartObjs = this.state.cart;
                var targetObj = cartObjs.find( (item) => item.product.id == target.id);
                var index = cartObjs.indexOf(targetObj);
                cartObjs[index].quantity = cartObjs[index].quantity + 1;
                this.setState({cart: cartObjs});
                break;
            default:
                break;
        }
    }

}
