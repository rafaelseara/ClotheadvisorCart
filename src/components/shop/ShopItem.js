import React, { Component } from 'react';
import './../../styles.css';

export default class ShopItem extends Component {


    render() {
        return (
            <div className="itemBox">
                <div className="itemDetails">
                    <h3>{this.props.name}</h3>
                    <p>{this.props.dsc}</p>
                </div>
                <div className="itemCartFooter">
                    <div className="leftAlign">
                        <button id={this.props.id} onClick={this.props.handleAdd}>Add to cart</button>
                    </div>
                    <div className="rightAlign">
                        <p>&euro;{this.props.price}</p>
                    </div>
                </div>
            </div>
        )
    }

}
