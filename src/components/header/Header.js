import React, { Component } from 'react';

import './../../styles.css';
import Breadcrumbs from './../breadcrumbs/Breadcrumbs'

export default class Header extends Component {

    render() {
        return (
            <header>
                <section>
                    <div>
                        <a href="https://rafaelseara.github.io">
                            Link to Rafael Seara Website
                        </a>
                        <h1>
                            Clotheadvisor Shopping Cart
                        </h1>
                    </div>
                </section>
                <Breadcrumbs/>
            </header>
        )
    }
}