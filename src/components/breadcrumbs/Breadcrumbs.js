import React, { Component } from 'react';

import './../../styles.css';

export default class Breadcrumbs extends Component {

    render() {
        return (
            <nav>
                <ul>
                    <li><a href="https://rafaelseara.github.io">Home</a></li>
                    <li>Shopping for the annual sailing trip</li>
                </ul>
            </nav>
        )
    }
}