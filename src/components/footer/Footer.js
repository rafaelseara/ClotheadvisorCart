import React, { Component } from 'react';
import './../../styles.css';

export default class Footer extends Component {

    render() {
        return (
            <footer className="footerDmlr">
                <a href="https://rafaelseara.github.io">
                Link to Rafael Seara Website
                </a>
                <label>&copy; 2018</label>
            </footer>
        )
    }
}



